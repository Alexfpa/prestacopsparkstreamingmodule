import java.util.Properties
import java.{lang, util}

import com.datastax.driver.core.{BoundStatement, PreparedStatement}
import com.datastax.spark.connector.SomeColumns
import com.datastax.spark.connector.cql.CassandraConnector
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.common.serialization.{StringDeserializer, StringSerializer}
import org.apache.spark._
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.streaming.dstream.{DStream, InputDStream}
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
import org.apache.spark.streaming.kafka010.{ConsumerStrategies, KafkaUtils, LocationStrategies}
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.slf4j.LoggerFactory
import com.datastax.spark.connector.streaming._
import org.apache.spark.sql.streaming.OutputMode
import org.apache.spark.sql.{DataFrame, SparkSession, functions}
import org.apache.spark.sql.types.{IntegerType, LongType, StringType, StructType}
import org.apache.spark.sql.cassandra._

import scala.util.parsing.json.JSONArray

object NYPoliceDepartmentDroneApp {

  def main(args: Array[String]):Unit={


    /*SparkSession.builder()
      .master("local[*]")
      .appName("PrestaCopJsonTest")
      .getOrCreate()
      .readStream
      .format("kafka")
      .option("kafka.bootstrap.servers","localhost:9092")
      .option("subscribe","drone_messages")
      .option("spark.locality.wait","1000")
      .option("spark.streaming.backpressure.enabled","true")
      .option("startingOffsets","earliest")
      .option("kafkaConsumer.pollTimeoutMs","512")
      .load()
      .selectExpr("CAST(value AS STRING)")
      .select(functions
        .from_json(functions.col("value").cast("string"), new StructType()
          .add("numberPlate", StringType)
          .add("lat", IntegerType)
          .add("lng", IntegerType)
          .add("time", LongType))
        .as("data")).select("data.*")
    //  .selectExpr("data.numberPlate", "data.lat","data.lng","data.time")
      .writeStream.format("console")
      .outputMode("append")
    .start()
      .awaitTermination()*/




    SparkSession.builder()
      .master("local[*]")
      .appName("PrestaCopJsonTest")
      .getOrCreate()
      .readStream
      .format("kafka")
      .option("kafka.bootstrap.servers","localhost:9092")
      .option("subscribe","drone_messages")
      .option("spark.locality.wait","1000")
      .option("spark.streaming.backpressure.enabled","true")
      .option("startingOffsets","earliest")
      .option("kafkaConsumer.pollTimeoutMs","512")
      .load()
      .selectExpr("CAST(value AS STRING)")
      .select(functions
        .from_json(functions.col("value").cast("string"), new StructType()
          .add("numberPlate", StringType)
          .add("lat", IntegerType)
          .add("lng", IntegerType)
          .add("time", LongType))
        .as("data"))
      .selectExpr("data.numberplate", "data.lat","data.lng","data.time")
    .writeStream
      .foreachBatch { (batchDF, _) =>
        batchDF
          .write
          .cassandraFormat("data", "drone_project")
          .mode("append")
          .save
      }.start().awaitTermination()


  }


}
