import org.apache.kafka.clients.producer.{KafkaProducer, Producer, ProducerRecord}
import java.io.Serializable
import java.util.Properties

class KafkaSink(createProducer: () => Producer[String, String]) extends Serializable {

  lazy val producer = createProducer()

  def send(topic: String, key: String, value: String): Unit = producer.send(new ProducerRecord(topic, key, value))
}

object KafkaSink {

  def apply(properties: Properties): KafkaSink = {
    val f = () => {
      val producer = new KafkaProducer[String, String](properties);

      sys.addShutdownHook {
        producer.close()
      }

      producer
    }
    new KafkaSink(f)
  }
}